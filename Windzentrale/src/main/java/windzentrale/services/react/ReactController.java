package windzentrale.services.react;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import windzentrale.services.data.model.WindzentraleData;

/**
 * @author Kacper Urbaniec
 * @version 2019-04-30
 */

@Controller
public class ReactController {

    @Autowired
    RestTemplate restTemplate;

    private final static String serviceUrl = "http://DATA-SERVICE";

    /*
    //@ResponseBody
    @RequestMapping("/")
    public String home() {
        return "index";
    }*/
    @RequestMapping(value = "/")
    public String index() {
        return "index";
    }

    @RequestMapping(value = "/data", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public WindzentraleData data() {
        /*String plainCreds = "123456789:1234";
        byte[] plainCredsBytes = plainCreds.getBytes();
        byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
        String base64Creds = new String(base64CredsBytes);
         */
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded");
        //headers.add("Authorization", "Basic " + base64Creds);
        HttpEntity<String> request = new HttpEntity<String>(headers);

        //restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("123456789", "1234"));
        /*ResponseEntity<String> result = restTemplate.exchange(serviceUrl + "/windzentrale/data_json", HttpMethod.POST, request,
                String.class);*/
        ResponseEntity<WindzentraleData> result = restTemplate.exchange(serviceUrl + "/windzentrale/data_json/dataService/ThisIsATotalSecurePasswd", HttpMethod.GET, request,
                WindzentraleData.class);
        WindzentraleData data = result.getBody();
        return data;
    }

}
