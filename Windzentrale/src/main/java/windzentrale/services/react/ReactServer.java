package windzentrale.services.react;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import windzentrale.services.user.UserRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

/**
 * Run as a micro-service, registering with the Discovery Server (Eureka).
 *
 * This class declares no beans and current package contains no components for
 * ComponentScan to find. No point using <tt>@SpringBootApplication</tt>.
 *
 * @author Kacper Urbaniec
 * @version 2019-04-30
 */
@EnableAutoConfiguration
@EnableDiscoveryClient
@Configuration
@ComponentScan
public class ReactServer {

    public static final String DATA_SERVICE_URL = "http://DATA-SERVICE";

    protected Logger logger = Logger.getLogger(ReactServer.class.getName());

    /**
     * Run the application using Spring Boot and an embedded servlet engine.
     *
     * @param args
     *            Program arguments - ignored.
     */
    public static void main(String[] args) {
        // Tell server to look for user-server.properties or
        // user-server.yml
        System.setProperty("spring.config.name", "react-server");

        SpringApplication.run(ReactServer.class, args);
    }

    @LoadBalanced //Needed to identify other services
    @Bean
    public RestTemplate restTemplate() {
        /*List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes(Collections.singletonList(MediaType.ALL));
        messageConverters.add(converter);*/
        RestTemplate restTemplate = new RestTemplate();
        //restTemplate.setMessageConverters(messageConverters);
        return restTemplate;
    }



}

