package windzentrale.services.data.mongo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import windzentrale.services.data.DataController;
import windzentrale.services.data.model.WindparkData;
import windzentrale.services.data.model.WindparkDataRepository;
import windzentrale.services.data.rest.RestService;


import java.util.LinkedList;
import java.util.logging.Logger;


/**
 * Saves all queued data that needs to be persist in MongoDB.
 * <br>
 * Data will be collected from {@link windzentrale.services.data.rest.RestConsumer,
 * which uses {@link MongoSaver#addData(windzentrale.services.data.model.WindparkData)} to hand the data
 * to this class.
 * <br>
 * @author Kacper Urbaniec
 * @version 08.03.2019
 */
@Component
@Scope("prototype")
public class MongoSaver implements Runnable{
    @Autowired
    private ApplicationContext context;

    @Autowired
    private RestService service;

    @Autowired
    private WindparkDataRepository repository;

    private boolean sending =  true;

    private LinkedList<WindparkData> stack;

    public MongoSaver() {
        stack = new LinkedList<>();
    }

    protected Logger logger = Logger.getLogger(DataController.class
            .getName());

    @Override
    public void run() {
        while (sending) {
            try {
                Thread.sleep(100);
                while (!stack.isEmpty()) {
                    WindparkData el = stack.pollFirst();
                    if (el != null)
                        repository.save(el);
                }
            }
            catch (Exception ex) {
                logger.warning(ex.getMessage());
            }
        }
    }

    public void addData(WindparkData data) {
        stack.add(data);
    }

    public void shutdown() {
        sending = false;
    }
}
