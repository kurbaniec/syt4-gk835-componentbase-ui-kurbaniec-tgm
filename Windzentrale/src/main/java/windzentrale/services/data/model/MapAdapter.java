package windzentrale.services.data.model;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Used for marshalling the HashMap of {@link WindzentraleData}.
 * @author Kacper Urbaniec
 * @version 10.03.2019
 */
public class MapAdapter extends XmlAdapter<MapElements[], Map<String, ArrayList<WindengineData>>> {
    public MapElements[] marshal(Map<String, ArrayList<WindengineData>> arg0) throws Exception {
        MapElements[] mapElements = new MapElements[arg0.size()];
        int i = 0;
        for (Map.Entry<String, ArrayList<WindengineData>> entry : arg0.entrySet())
            mapElements[i++] = new MapElements(entry.getKey(), entry.getValue());

        return mapElements;
    }

    public Map<String, ArrayList<WindengineData>> unmarshal(MapElements[] arg0) throws Exception {
        Map<String, ArrayList<WindengineData>> r = new HashMap<>();
        for (MapElements mapelement : arg0)
            r.put(mapelement.key, mapelement.value);
        return r;
    }
}
