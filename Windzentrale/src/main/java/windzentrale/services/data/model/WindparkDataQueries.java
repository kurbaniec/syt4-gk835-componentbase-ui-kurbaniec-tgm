package windzentrale.services.data.model;

import java.util.List;

/**
 * Used to extend the MongoRepository {@link WindparkDataRepository} with more functions like
 * aggregate-queries.
 *
 * @author Kacper Urbaniec
 * @version 2019-04-16
 */

public interface WindparkDataQueries {

    public List<WindengineData> findEngineDataByWindpark(String windengineID, String WindparkID);

    public WindzentraleData findDataByWindpark(String WinparkID);

    public WindzentraleData findAllData();

}
