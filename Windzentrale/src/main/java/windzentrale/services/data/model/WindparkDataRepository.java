package windzentrale.services.data.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Context;
import java.util.List;

/**
 * Basic MongoRepository, uses {@link WindparkDataQueries} for more complex functions.
 *
 * @author Kacper Urbaniec
 * @version 05.03.2019
 */
public interface WindparkDataRepository extends MongoRepository<WindparkData, String> , WindparkDataQueries {

    public List<WindparkData> findByWindparkID(String windparkID);

}

