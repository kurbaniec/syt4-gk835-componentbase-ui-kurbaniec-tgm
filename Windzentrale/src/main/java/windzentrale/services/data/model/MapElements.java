package windzentrale.services.data.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;

/**
 * Used for marshalling the HashMap of {@link WindzentraleData}.
 * @author Kacper Urbaniec
 * @version 10.03.2019
 */
@XmlAccessorType(XmlAccessType.NONE)
public class MapElements {

    public String key;

    public ArrayList<WindengineData> value;

    public MapElements(String key, ArrayList<WindengineData> value) {
        this.key = key;
        this.value = value;
    }

    public MapElements() {}

    @XmlElement(name = "ID")
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @XmlElement(name = "WindengineData")
    public ArrayList<WindengineData> getValue() {
        return value;
    }

    public void setValue(ArrayList<WindengineData> value) {
        this.value = value;
    }
}
