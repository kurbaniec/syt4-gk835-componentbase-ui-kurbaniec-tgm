package windzentrale.services.data.model;

import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Basic data structure that holds Windengine information.
 */

@XmlRootElement
public class WindengineData implements Comparable<WindengineData>{

	private String windengineID;
	private String timestamp;
	
	private double windspeed;
	private String unitWindspeed;

	private double temperature;
	private String unitTemperature;

	private double power;
	private String unitPower;

	private double blindpower;
	private String unitBlindpower;

	private double rotationspeed;
	private String unitRotationspeed;

	private double bladeposition;
	private String unitBladeposition;

	/**
	 * Constructor
	 */
	public WindengineData() {
		
		this.timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date());
		this.unitWindspeed = "kmH";
		this.unitTemperature = "C";
		this.unitPower = "kwH";
		this.unitBlindpower = "kwH";
		this.unitRotationspeed = "uM";
		this.unitBladeposition = "grad";
		
	}


	/**
	 * Setter and Getter Methods
	 */

	public void setUnitWindspeed(String unitWindspeed) {
		this.unitWindspeed = unitWindspeed;
	}

	public void setUnitTemperature(String unitTemperature) {
		this.unitTemperature = unitTemperature;
	}

	public void setUnitPower(String unitPower) {
		this.unitPower = unitPower;
	}

	public void setUnitBlindpower(String unitBlindpower) {
		this.unitBlindpower = unitBlindpower;
	}

	public void setUnitRotationspeed(String unitRotationspeed) {
		this.unitRotationspeed = unitRotationspeed;
	}

	public void setUnitBladeposition(String unitBladeposition) {
		this.unitBladeposition = unitBladeposition;
	}

	public String getWindengineID() {
		return windengineID;
	}
	
	public void setWindengineID(String windengineID) {
		this.windengineID = windengineID;
	}
	
	public String getTimestamp() {
		return timestamp;
	}
	
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
	public void setWindspeed(double windspeed) {
		this.windspeed = windspeed;
	}
	
	public double getWindspeed() {
		return windspeed;
	}
	
	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	public double getPower() {
		return power;
	}

	public void setPower(double power) {
		this.power = power;
	}

	public double getBlindpower() {
		return blindpower;
	}

	public void setBlindpower(double blindpower) {
		this.blindpower = blindpower;
	}

	public double getRotationspeed() {
		return rotationspeed;
	}

	public void setRotationspeed(double rotationspeed) {
		this.rotationspeed = rotationspeed;
	}

	public double getBladeposition() {
		return bladeposition;
	}

	public void setBladeposition(double bladeposition) {
		this.bladeposition = bladeposition;
	}

	public String getUnitWindspeed() {
		return unitWindspeed;
	}

	public String getUnitTemperature() {
		return unitTemperature;
	}

	public String getUnitPower() {
		return unitPower;
	}

	public String getUnitBlindpower() {
		return unitBlindpower;
	}

	public String getUnitRotationspeed() {
		return unitRotationspeed;
	}

	public String getUnitBladeposition() {
		return unitBladeposition;
	}

	/**
	 * Methods
	 */
	@Override
	public String toString() {
		String info = String.format("Windengine Info: ID = %s, timestamp = %s, windspeed = %f",
			windengineID, timestamp, windspeed );
		return info;
	}

    @Override
    public int compareTo(WindengineData o) {
        // 2019-03-05 11:32:52.398
        Timestamp t1 = Timestamp.valueOf(this.timestamp), t2 = Timestamp.valueOf(o.getTimestamp());
        if(t1.before(t2)) {
            return 1;
        }
        else if(t1.after(t2)) {
            return -1;
        }
        else return 0;
    }
}
