package windzentrale.services.data.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Data structure that holds all data from all windparks in a HashMap.
 * @author Kacper Urbaniec
 * @version 21.01.2019
 */
@XmlRootElement(name = "windzentrale")
public class WindzentraleData {


    private HashMap<String, ArrayList<WindengineData>> zentrale;

    public WindzentraleData(HashMap<String, ArrayList<WindengineData>> zentrale) {
        this.zentrale = zentrale;
    }

    public WindzentraleData() {}

    @XmlJavaTypeAdapter(MapAdapter.class)
    @XmlElement(name="Windparks")
    public HashMap<String, ArrayList<WindengineData>> getZentrale() {
        return zentrale;
    }

    public void setZentrale(HashMap<String, ArrayList<WindengineData>> zentrale) {
        this.zentrale = zentrale;
    }

    public ArrayList<WindengineData> getWindengineData() {
        ArrayList<WindengineData> resultSet = new ArrayList<>();
        for (String key : zentrale.keySet()) {
            resultSet.addAll(zentrale.get(key));
        }
        return resultSet;
    }




    public int size() {
        return getWindengineData().size();
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("Windzentrale\n");
        for(String key: zentrale.keySet()) {
            str.append("Windpark ").append(key).append("\n");
            for(WindengineData w: zentrale.get(key)) {
                str.append(w.toString()).append("\n");
            }
        }
        return str.toString();
    }

}