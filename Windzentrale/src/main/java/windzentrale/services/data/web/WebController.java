package windzentrale.services.data.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import windzentrale.services.data.model.WindengineData;
import windzentrale.services.data.model.WindparkDataRepository;
import windzentrale.services.data.model.WindzentraleData;

import java.util.List;
import java.util.logging.Logger;

/**
 * Client controller, fetches data info from MongoDB via
 * {@link WindparkDataRepository}.
 * 
 * @author Kacper Urbaniec
 * @version 2019-04-17
 */
@Controller
public class WebController {

	@Autowired
	protected WindparkDataRepository repository;

	protected Logger logger = Logger.getLogger(WebController.class
			.getName());

	public WebController() {}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.setAllowedFields("windparkField", "windengineField");
	}

	@RequestMapping("/data")
	public String goHome() {
		return "index";
	}

	@RequestMapping("/data/windengine/{windparkID}/{windengineID}")
	public String windengineSearch(Model model, @PathVariable("windparkID") String windparkID,
								   @PathVariable("windengineID") String windengineID) {
		logger.info("Data-Service windengineSearch() invoked: " + windparkID + " / " + windengineID);
		List<WindengineData> data = repository.findEngineDataByWindpark(windengineID, windparkID);
		logger.info("Data-Service windengineSearch() found " + data.size() + " data records");
		model.addAttribute("windparkID", windparkID);
		model.addAttribute("windengineID", windengineID);
		if (data != null)
			model.addAttribute("data", data);
		return "data";
	}

	@RequestMapping("/data/windpark/{windparkID}")
	public String windparkSearch(Model model, @PathVariable("windparkID") String windparkID) {
		logger.info("Data-Service windparkSearch() invoked: " + windparkID);
		WindzentraleData data = repository.findDataByWindpark(windparkID);
		logger.info("Data-Service windparkSearch() found " + data.size() + " data records");
		model.addAttribute("windparkID", windparkID);
		model.addAttribute("windengineID", "all");
		model.addAttribute("type", "windpark");
		if (data != null)
			model.addAttribute("data", data);
		return "datav";
	}

	@RequestMapping("/data/windzentrale/verbose")
	public String windzentraleSearch(Model model) {
		logger.info("Data-Service windzentraleSearch() invoked");
		WindzentraleData data = repository.findAllData();
		logger.info("Data-Service windzentraleSearch() found " + data.size() + " data records");
		model.addAttribute("windparkID", "0");
		model.addAttribute("windengineID", "all");
		model.addAttribute("type", "windzentrale");
		if (data != null)
			model.addAttribute("data", data);
		return "datav";
	}

	@RequestMapping(value = "/data/search", method = RequestMethod.GET)
	public String searchForm(Model model) {
		model.addAttribute("searchCriteria", new SearchCriteria());
		return "dataSearch";
	}

	@RequestMapping(value = "/data/dosearch")
	public String doSearch(Model model, SearchCriteria criteria,
			BindingResult result) {
		logger.info("Data-Service search() invoked: " + criteria);

		criteria.validate(result);

		if (result.hasErrors())
			return "dataSearch";

		String windparkID = criteria.getWindparkField();
		String windengineID = criteria.getWindengineField();
		if (!StringUtils.hasText(windparkID) && !StringUtils.hasText(windengineID)) {
			return windzentraleSearch(model);
		} else if (StringUtils.hasText(windparkID) && StringUtils.hasText(windengineID)) {
			return windengineSearch(model, windparkID, windengineID);
		}
		else return windparkSearch(model, windparkID);
	}
}
