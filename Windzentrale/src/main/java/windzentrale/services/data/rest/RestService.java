package windzentrale.services.data.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import windzentrale.services.data.DataController;
import windzentrale.services.data.model.WindparkDataRepository;
import windzentrale.services.data.mongo.MongoSaver;


import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Starts a {@link MongoSaver}-Thread and all needed {@link RestConsumer}-Threads.
 * <br>
 * @author Kacper Urbaniec
 * @version 08.03.2019
 */
@Service
@Component
@Configurable
public class RestService {

    @Autowired
    ApplicationContext context;

    @Autowired
    TaskExecutor taskExecutor;

    @Autowired
    private WindparkDataRepository repository;

    @Value("${windpark.count}")
    private String windCountString;

    private int windCount;

    private ArrayList<RestConsumer> consumerThreads = new ArrayList<>();

    protected Logger logger = Logger.getLogger(DataController.class
            .getName());

    @Bean
    public ThreadPoolTaskExecutor taskExecutor(){
        ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
        pool.setCorePoolSize(4);
        pool.setMaxPoolSize(10);
        pool.setWaitForTasksToCompleteOnShutdown(true);
        return pool;
    }

    @PostConstruct
    public void executeAsynchronously() {
        // Output all saved data:
        /*
        System.out.println("#####################");
        logger.info("Saved Data:\n");
        for(WindparkData d : repository.findAll()) {
            logger.info("\tData: " + d);
        }
        System.out.println("#####################\n\n");
        */

        // Start threads
        windCount = Integer.parseInt(windCountString);

        System.out.println("Starting MongoSaver");
        MongoSaver mongo = context.getBean(MongoSaver.class);
        taskExecutor.execute(mongo);

        logger.info("RestService: Starting " + windCount + " RestConsumer-Thread(s)");
        int port = 8080;

        for(int i = 0; i < windCount; i++) {
            RestConsumer consumer = context.getBean(RestConsumer.class, mongo, port);
            consumerThreads.add(consumer);
            // Execute Threads
            taskExecutor.execute(consumer);
            port += 100;
        }
    }
}

