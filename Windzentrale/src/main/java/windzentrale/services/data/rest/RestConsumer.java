package windzentrale.services.data.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import windzentrale.services.data.DataController;
import windzentrale.services.data.model.WindparkData;
import windzentrale.services.data.mongo.MongoSaver;

import java.util.logging.Logger;


/**
 * Collects every three seconds data from a windpark,
 * that needs to be saved in the windzentrale via {@link MongoSaver}.
 * <br>
 * @author Kacper Urbaniec
 * @version 8.03.2019
 */
@Component
@Scope("prototype")
public class RestConsumer implements Runnable{
    /*@Autowired
    private ApplicationContext context;

    @Autowired
    private RestService service;

    @Autowired
    private MongoRepository repository;*/

    private boolean sending =  true;

    private ObjectMapper mapper;

    private MongoSaver mongo;

    private int port;

    private int timeout = 3000;

    protected Logger logger = Logger.getLogger(DataController.class
            .getName());

    public RestConsumer(MongoSaver mongo, int port) {
        this.mongo = mongo;
        this.port = port;
    }

    @Override
    public void run() {
        while (sending) {
            RestTemplate rest = new RestTemplate();
            try {
                WindparkData data =
                        rest.getForObject(
                                "http://localhost:"+port+"/windpark/data_json",
                                WindparkData.class
                        );
                mongo.addData(data);
                logger.info("MongoSaver[" + Thread.currentThread().getName()
                        + "] - Adding Data: " + data.toString());
                timeout = 3000;
                Thread.sleep(timeout);
            }
            catch (Exception ex) {
                logger.warning("Mongo Saver[" + Thread.currentThread().getName()
                        + "] - " + ex.getMessage());
                try {
                    Thread.sleep(timeout);
                    if (timeout < 600000) {
                        if (timeout * 2 < 600000) {
                            timeout *= 2;
                        }
                        else timeout = 600000;
                        logger.info("Mongo Saver[" + Thread.currentThread().getName()
                                + "] - Timeout set to " + timeout + " ms");
                    }
                } catch (Exception ex2) {
                    logger.warning("Mongo Saver[" + Thread.currentThread().getName()
                            + "] - " + ex2.getMessage());
                }
            }
        }
    }

    public void shutdown() {
        sending = false;
    }
}
