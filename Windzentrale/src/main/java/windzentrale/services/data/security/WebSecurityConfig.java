package windzentrale.services.data.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Defines which URL paths need to be secured. Also enables WebSecurity.
 * @author Kacper Urbaniec
 * @version 2019-04-17
 */

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .csrf().disable()
            .authorizeRequests()
                .antMatchers("/windzentrale/data_json/*/*").permitAll()
                .antMatchers( "/resources/public/**", "/resources/**").permitAll()
                .anyRequest().fullyAuthenticated()
                //.anyRequest().permitAll() // Comment line before and uncomment this for no security
                .and()
            .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
            .logout()
                .permitAll();
    }

}