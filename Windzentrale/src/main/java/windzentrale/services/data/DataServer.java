package windzentrale.services.data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import java.util.logging.Logger;

/**
 * Run as a micro-service, registering with the Discovery Server (Eureka).
 *
 * Used as a service for finding data from all the windparks.
 * 
 * @author Kacper Urbaniec
 * @version 2019-04-15
 */


@EnableDiscoveryClient
@SpringBootApplication
public class DataServer {


	public static final String USER_SERVICE_URL = "http://USER-SERVICE";

	@LoadBalanced
	@Bean
	RestTemplate restTemplate() {
		return new RestTemplate();
	}

	protected Logger logger = Logger.getLogger(DataServer.class.getName());

	/**
	 * Run the application using Spring Boot and an embedded servlet engine.
	 * 
	 * @param args
	 *            Program arguments - ignored.
	 */
	public static void main(String[] args) {
		// Tell server to look for user-server.properties or
		// user-server.yml
		System.setProperty("spring.config.name", "data-server");

		SpringApplication.run(DataServer.class, args);

	}

}
