package windzentrale.services.user;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class for the model "User", that is used to store User-information.
 *
 * @author Kacper Urbaniec
 * @version 2019-04-15
 */

@XmlRootElement(name = "User")
public class User {

    @XmlElement
    private String number;

    @XmlElement
    private String owner;

    @XmlElement
    private String passwd;

    public User(String number, String owner, String passwd) {
        this.number = number;
        this.owner = owner;
        this.passwd = passwd;
    }

    public User() {}

    public String getnumber() {
        return number;
    }

    public void setnumber(String number) {
        this.number = number;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    @Override
    public String toString() {
        return number + ": " + owner;
    }
}
