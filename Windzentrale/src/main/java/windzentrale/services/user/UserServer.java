package windzentrale.services.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.logging.Logger;

/**
 * Run as a micro-service, registering with the Discovery Server (Eureka).
 *
 * This class declares no beans and current package contains no components for
 * ComponentScan to find. No point using <tt>@SpringBootApplication</tt>.
 * 
 * @author Kacper Urbaniec
 * @version 2019-04-15
 */
@EnableAutoConfiguration
@EnableDiscoveryClient
@Configuration
@ComponentScan
public class UserServer {

	@Autowired
	protected UserRepository userRepository;

	protected Logger logger = Logger.getLogger(UserServer.class.getName());

	/**
	 * Run the application using Spring Boot and an embedded servlet engine.
	 * 
	 * @param args
	 *            Program arguments - ignored.
	 */
	public static void main(String[] args) {
		// Tell server to look for user-server.properties or
		// user-server.yml
		System.setProperty("spring.config.name", "user-server");

		SpringApplication.run(UserServer.class, args);
	}
}
