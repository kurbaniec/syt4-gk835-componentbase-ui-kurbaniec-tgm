package windzentrale.services.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import windzentrale.exceptions.AccountNotFoundException;

import java.util.List;
import java.util.logging.Logger;

/**
 * A RESTFul controller for accessing user information.
 *
 * @author Kacper Urbaniec
 * @version 2019-04-15
 */
@RestController
public class UserController {

	protected Logger logger = Logger.getLogger(UserController.class
			.getName());
	protected UserRepository userRepository;

	/**
	 * Create an instance plugging in the respository of Accounts.
	 *
	 * @param userRepository
	 *            An account repository implementation.
	 */
	@Autowired
	public UserController(UserRepository userRepository) {
		this.userRepository = userRepository;

		logger.info("UserRepository says system has "
				+ userRepository.countAll() + " user");
	}

	/**
	 * Fetch an account with the specified account number.
	 * 
	 * @param accountNumber
	 *            A numeric, 9 digit account number.
	 * @return The account if found.
	 * @throws AccountNotFoundException
	 *             If the number is not recognised.
	 */
	@RequestMapping("/users/{accountNumber}")
	public User byNumber(@PathVariable("accountNumber") String accountNumber) {

		logger.info("user-service byNumber() invoked: " + accountNumber);
		User user = userRepository.findByNumber(accountNumber);
		logger.info("user-service byNumber() found: " + user);

		if (user == null)
			throw new AccountNotFoundException(accountNumber);
		else {
			return user;
		}
	}

	/**
	 * Fetch user with the specified name. A partial case-insensitive match
	 * is supported. So <code>http://.../accounts/owner/a</code> will find any
	 * user with upper or lower case 'a' in their name.
	 * 
	 * @param partialName
	 * @return A non-null, non-empty set of user.
	 * @throws AccountNotFoundException
	 *             If there are no matches at all.
	 */
	@RequestMapping("/users/owner/{name}")
	public List<User> byOwner(@PathVariable("name") String partialName) {
		logger.info("user-service byOwner() invoked: "
				+ userRepository.getClass().getName() + " for "
				+ partialName);

		List<User> users = userRepository
				.findByOwnerContainingIgnoreCase(partialName);
		logger.info("user-service byOwner() found: " + users);

		if (users == null || users.size() == 0)
			throw new AccountNotFoundException(partialName);
		else {
			return users;
		}
	}

	@RequestMapping(value = "/auth/user/{name}/password/{passwd}",
			method = RequestMethod.POST, produces="application/json")
	public boolean authentication(@PathVariable("name") String username,
								  @PathVariable("passwd") String password) {
		logger.info("Authentication for User: " + username);
		try {
			User user = userRepository.findByNumber(username);
			return (user.getPasswd().equals(password));
		}
		catch (Exception ex) {
			logger.warning("Authentication Error: " + ex.getMessage());
		}
		return false;
	}
}
