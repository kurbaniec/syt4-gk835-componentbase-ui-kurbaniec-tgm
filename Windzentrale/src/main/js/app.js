
'use strict';

import update from "immutability-helper";
import Container from 'react-bootstrap/Container'
import Table from 'react-bootstrap/Table'
import Form from 'react-bootstrap/Form'
import Col from 'react-bootstrap/Col'


const React = require('react');
const ReactDOM = require('react-dom');
const client = require('./client');


/**
 * My App that display Windzentrale Data.
 */
class App extends React.Component {

	constructor(props) {
		console.log("Starting App");
		super(props);
		this.state = {
			cent: [],
			filter: {parkID: "", engineID: "", time: "", power:""}
		};
	}

	/**
	 * Request Data via Rest.
	 * Data is delivered asynchronously.
	 */
	componentDidMount() {
		console.log("Requesting Data");
		client({method: 'GET', path: '/data'}).done(response => {
			this.setState({cent: response.entity.zentrale});
		});
	}

	render() {
		const {filter}= this.state;
		return (
			<div>
			<Container>
				<h1>Windzentrale Data</h1>
				<h3>Data presented via React</h3>
				<br/>
				<Form>
					<h4>Filter with following parameters</h4>
					<Form.Row>
						<Form.Group as={Col} controlId="formGridWindparkID">
							<Form.Label>Windpark ID</Form.Label>
							<Form.Control type="text" placeholder="Enter Windpark ID" value={ filter.parkID }
										  onChange={ e => {
											  this.setState({
												  filter: update(
													  this.state.filter, {parkID: {$set: e.target.value}})
											  });
										  }} />
						</Form.Group>
						<Form.Group as={Col} controlId="formGridWindengineID">
							<Form.Label>Windengine ID</Form.Label>
							<Form.Control type="text" placeholder="Enter WindengineID" value={ filter.engineID }
										  onChange={ e => {
											  this.setState({
												  filter: update(
													  this.state.filter, {engineID: {$set: e.target.value}})
											  });
										  }} />
						</Form.Group>

						<Form.Group as={Col} controlId="formGridTimestamp">
							<Form.Label>Timestamp</Form.Label>
							<Form.Control type="text" placeholder="Enter Timestamp" value={ filter.time }
										  onChange={ e => {
											  this.setState({
												  filter: update(
													  this.state.filter, {time: {$set: e.target.value}})
											  });
										  }} />
						</Form.Group>
						<Form.Group as={Col} controlId="formGridPower">
							<Form.Label>Power</Form.Label>
							<Form.Control type="text" placeholder="Enter Power" value={ filter.power }
										  onChange={ e => {
											  this.setState({
												  filter: update(
													  this.state.filter, {power: {$set: e.target.value}})
											  });
										  }} />
						</Form.Group>
					</Form.Row>
				</Form>
				<Parks cent={this.state.cent} filter={this.state.filter}/>
			</Container>
			</div>
		)
	}
}

/**
 * Class that displays all Windparks.
 */
class Parks extends React.Component{
	constructor(props) {
		console.log("Creating Parks Component");
		super(props);
	}

	render() {
		console.log("Building WindengineLists for every Park");
		const parks = Object.keys(this.props.cent).map(key =>
			<EngineList key={key} parkID={key} data={this.props.cent[key]} filter={this.props.filter}/>
		);
		return (
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>Timestamp</th>
						<th>WindparkID</th>
						<th>WindengineID</th>
						<th>Windspeed</th>
						<th>Rotationspeed</th>
						<th>Bladeposition </th>
						<th>Temperature </th>
						<th>Power </th>
						<th>Blindpower </th>
					</tr>
				</thead>
				<tbody>
					{parks}
				</tbody>
			</Table>
		)
	}
}


/**
 * Class that displays all Engines.
 */
class EngineList extends React.Component{
	render() {
		console.log("Creating Engines for Park " + this.props.parkID);
		const filter = this.props.filter;
		const engineList = Object.keys(this.props.data).map(key => {
				if (this.props.parkID.includes(filter.parkID)
					&& this.props.data[key].windengineID.includes(filter.engineID)
					&& this.props.data[key].timestamp.includes(filter.time)
					&& String(this.props.data[key].power).includes(filter.power)

				) {
					return <Engine key={key} parkID={this.props.parkID} engine={this.props.data[key]} filter={this.props.filter}/>
				}
			}
		);
		return (
			engineList
		)
	}
}

/**
 * Class that display one Engine.
 */
class Engine extends React.Component{
	render() {
		return (
			<tr>
				<td>{this.props.engine.timestamp}</td>
				<td>{this.props.parkID}</td>
				<td>{this.props.engine.windengineID}</td>
				<td>{this.props.engine.windspeed + " " + this.props.engine.unitWindspeed}</td>
				<td>{this.props.engine.rotationspeed + " " + this.props.engine.unitRotationspeed}</td>
				<td>{this.props.engine.bladeposition + " " + this.props.engine.unitBladeposition}</td>
				<td>{this.props.engine.temperature + " " + this.props.engine.unitTemperature}</td>
				<td>{this.props.engine.power + " " + this.props.engine.unitPower}</td>
				<td>{this.props.engine.blindpower + " " + this.props.engine.unitBlindpower}</td>
			</tr>
		)
	}
}


ReactDOM.render(
	<App />,
	document.getElementById('react')
)

