# Middleware Engineering "Componentbase UI with Spring React"

## Aufgabenstellung
Die detaillierte [Aufgabenstellung](TASK.md) beschreibt die notwendigen Schritte zur Realisierung.

## Implementierung

Anfangs war das Problem das der Example Code sind nicht builden ließ, es gab Probleme mit dem Node Packet Manager (NPM), er konnte einfach das benötigte, runtergeladene Archiv mit Funktionen entpacken. Ohne erfolgreichen Build wurde auf index-Seite, die mit React basierte, nur der Fehler `built/bundle.js` angezeigt.

Der Grund für das Fehlschlagen des Build-Vorgangs war, dass über das Schul-WLAN korrupte Daten heruntergeladen worden sind, die NPM nicht erfolgreich installierten. 

Um dieses Problem zu lösen, muss man einfach alle heruntergeladenen Dependencies zu *node* und *npm* löschen. Diese befinden sich im `eirslett`-Ordner, der bei mir unter `C:\Users\aon91\.m2\repository\com\github\eirslett` zu finden war. Nach Löschen und erneuten ausführen des Build-Vorganges funktionierte endlich dieses und die fehlende `bundle.js` wurde endlich komplett erstellt.

---

### "*Employee Overview*"

Das Beispiel [*Employee Overview*](https://spring.io/guides/tutorials/react-and-spring-data-rest/) zeigt wie man Daten per REST auf einer Website basierend auf React erstellt, die zusätzliche Funktionen wie Filter implementiert. Im Hintergrund wird immer noch alles per Spring verwaltet, also die REST-Schnittstelle und der Tomcat-Server.

Anfangs wird ein einfaches Repository für eine Datenbank (H2) in Spring implementiert, die später die Daten per REST-Schnittstelle ausgibt, die React konsumieren wird. 

Dann wird ein einfacher Controller erstellt, der eine Index-Seite zurückgibt. Diese beinhaltet ein wichtiges Tag: `<div id="react"></div>`. Damit wird gesagt, dass hier React aus `app.js` geladen werden soll. Damit dies funktioniert muss die `pom.xml` angepasst werdem, außerdem müssen die Dependencies für React via `pacakge.json` und `webpack.config.js` angegeben werden. Die genau Konfiguration ist im Tutorial zu finden.

Der nächste Schritt im Tutorial war schon die Erstellung der Logik mittels React. Diese wird in `app.js` definiert. Das Beispiel zeigt, wie eine Liste aus Benutzern aus der am Anfang erstellten REST-Schnittstelle geholt werden und dann mittels sogenannter React *Componets* angezeigt werden. Diese Komponenten haben Konstruktoren, Attribute (veränderliche *States* und nicht veränderbare *Properties*) und ganz wichtig eine Methode `render()`, diese Methode gibt an was auf unserer Seite angezeigt werden soll. 

Das Tutorial geht weiter, für den Zweck dieser Aufgaben genügt es aber.

---

### Implementierung der Aufgabe

Als Basis für die Aufgabe habe ich nicht die "*Document Oriented Middleware using MongoDB"*-Aufgabe genommen, sondern die neuere "*Middleware Engineering Microservices*". 

Für React habe ich einfach einen eigenen Microservice erstellt, dieser holt sich die Daten für die Website aus einer REST-Schnittstelle aus dem "*Data-Service*".

Anfangs hatte ich das Problem, die im Tutorial verwendete Methoden zum Holen der Daten aus React zu benutzen, weil diese mir dauern den Fehler *406* wurf, also das die Daten in einer nicht passenden Form geliefert wurden, obwohl es einfache JSON-Schnittstelle war. Ich verwendete dann die `fetch`-Funktion aus Standard-Javascript, diese ist aber eine umständlichere asynchrone Funktion, die teilweise die Programmlogik unberechenbar. Die im Tutorial verwendete `client`-Methode funktioniert auch asynchron, ist es aber schneller fertig und berechenbarer. 

Ich löste das Problem, indem ich im `client.js`-File, das die namengleiche Funktion konfiguriert, alle *accept* bzw. *require*-Attribute von `application/hal+json` auf `application/json` änderte. Hypertext Application Language (HAL) ist eine neue, in Arbeit befindene  Form Daten zu übertrag. Wahrscheinlich konnte sie mein JSON nicht auf diese Konvention übersetzten und schlug fehl.

Jetzt funktionierte die Daten-Einholung per `client`:

```react
componentDidMount() {
    console.log("Requesting Data");
    client({method: 'GET', path: '/data'}).done(response => {
        this.setState({cent: response.entity.zentrale});
    });
}
```

*Auszug aus App (app.js)*

Bevor ich weiter gehe, möchte ich noch hinweisen, dass man Spring nicht neu laden muss, um React-Änderungen zu builden. Mit der Konfiguration aus dem Tutorial muss man nur `mvn clean package` laufen lassen, dann bildet sich React neu und man kann die Startseite einfach nur im Browser neu Laden um die Änderungen zu sehen.

Meine App besteht aus den Komponenten `App`, `Parks`, `EngineList` und `Engine`. 

`App` besitzt das grundlegende Layout, die Funktion die Daten per REST zu konsumieren. Außerdem verfügt sie über die State-Attribute `cent` und `filter`. `cent` speichert die konsumierten Daten per REST, die hierarchisch an  `Parks`, `EngineList` und `Engine` weitergeben werden. `Parks` erzeugt zwei  `EngineList`-Objekte pro Windpark, die wiederum alle Windkraftdaten per  `Engine` zurückgeben.  Die Weitergabe erfolgt einfach im Konstruktor z.B. hier übergibt die `App` dem `Parks`-Objekt alle Daten und den derzeitigen Filter: 

```react
<Parks cent={this.state.cent} filter={this.state.filter}/>
```

Dadurch kann z.B. `Parks`  auf die Daten per `this.props.cent` zugreifen und für jeden Windpark ein `EngineList`-Objekt erstellen:

```react
const parks = Object.keys(this.props.cent).map(key =>
	<EngineList key={key} parkID={key} data={this.props.cent[key]} filter={this.props.filter}/>
);
```

*Auszug aus Parks*

Bei der Klasse `EngineList` wird es interessant, den hier werden übergebenen Filter endlich eingesetzt. Mit einer *If*-Anweisung wird geschaut, ob die die einzelnen Windengines den Kriterien entsprechen. Wenn ja, werden sie einer Konstante `engineList` hinzugefügt, die Am Schluss zurückgeben wird.

```react
render() {
		console.log("Creating Engines for Park " + this.props.parkID);
		const filter = this.props.filter;
		const engineList = Object.keys(this.props.data).map(key => {
				if (this.props.parkID.includes(filter.parkID)
					&& this.props.data[key].windengineID.includes(filter.engineID)
					&& this.props.data[key].timestamp.includes(filter.time)
					&& String(this.props.data[key].power).includes(filter.power)

				) {
					return <Engine key={key} parkID={this.props.parkID} engine={this.props.data[key]} filter={this.props.filter}/>
				}
			}
		);
		return (
			engineList
		)
}
```

*Auszug aus EngineList*

Wenn die Filter-Kriterien für eine einzelne Windanlage übereinstimmen, wird einfach einfach ein Daten-Tag für eine Tabelle definiert:

```react
render() {
		return (
			<tr>
				<td>{this.props.engine.timestamp}</td>
				<td>{this.props.parkID}</td>
				<td>{this.props.engine.windengineID}</td>
				<td>{this.props.engine.windspeed + " " + this.props.engine.unitWindspeed}</td>
				<td>{this.props.engine.rotationspeed + " " + this.props.engine.unitRotationspeed}</td>
				<td>{this.props.engine.bladeposition + " " + this.props.engine.unitBladeposition}</td>
				<td>{this.props.engine.temperature + " " + this.props.engine.unitTemperature}</td>
				<td>{this.props.engine.power + " " + this.props.engine.unitPower}</td>
				<td>{this.props.engine.blindpower + " " + this.props.engine.unitBlindpower}</td>
			</tr>
		)
}
```

*Auszug aus Engine*

Was noch erwähnt werden muss, ist wie der Filter am Anfang funktioniert. Die `App`-Komponente definiert einen State namens `filter` (`filter: {parkID: "", engineID: "", time: "", power:""}`) . Dieser ist ein Objekt und enthält vier Filter-Möglichkeiten. Diese werden jeweils einen Input-Element hinzugefügt, der bei Veränderung den String dieser ändert, der anfangs ja leer ist, weil nichts gefiltert werden soll. Dann muss ein mühseliger `setState` Methoden-Aufruf gemacht werden, der nur einen String des Objektes ändert (hier `parkID`) und dadurch zwingt alle Komponenten neu zu laden.

```react
<Form.Control type="text" placeholder="Enter Windpark ID" value={ filter.parkID }
    onChange={ e => {
        this.setState({
            filter: update(
                this.state.filter, {parkID: {$set: e.target.value}})
        });
    }} />
```

*Auszug aus App*

Schlussendlich sieht die Seite so aus, sie bietet die Möglichkeit nach Windpark, Windanlage, Zeit und Leistung zu filtern. Das Design habe ich ein bisschen mithilfe von Bootstrap verschönert.

![Fertige Seite](img/Output.PNG)

*Fertige React Seite*

**Anmerkung**: Für Bootstrap oder andere Dependencies muss immer `package.json` aktualisiert werden, ab und zu muss `webpack.config.js` geändert werden, falls Module beim Start geladen werden sollen. In den Files, die diese Dependencies verwenden müssen am Anfang immer diese per `import` geladen werden. React empfiehlt nicht alles zu landen, sondern nur die Funktionen die man wirklich braucht.

## Fragestellungen

- Was ist React?     
  React ist eine JavaScript-Library, die dem Entwickler gewisse Elemente bereitstellt, die ein Grundgerüst für die Ausgabe von Komponenten auf einer Website darstellen.   

- React untertützt eine komponentenbasierte Entwicklung. Beschreiben Sie den Begriff "Component" im Zusammenhang mit React.      
  *Components* sind eigens definierte, wieder-verwendbare HTML-Elemente, die benutzt werden können, um einen schnelles und effizientes Interfaces zu bauen. Komponenten können auch Daten mittels veränderlichen *States* und weniger veränderlichen *Properties* speichern.   

- Wozu wird die Klasse React.Component verwendet?       
  Alle Komponenten müssen von der Klasse React.Component extenden, um ein eigene Komponente für React darzustellen. Jede Komponente muss, um funktionsfähig zu sein, die Methode `render()` implementieren. Wie schon vorher erwähnt sind Komponenten kleine, abhängigkeitslose Elemente einer UI, die wieder verwendbar sind.   
  
- Welche Teile der Applikation werden in der Funktion render() implementiert?      
  In der Render-Funktion wird die Anzeige der Applikation definiert, also die *View*. Jede Komponente hat ihre eigenen render-Methode.  

- Vervollständigen Sie die angeführten Sätze:
  - React is a .................. - one of the most popular ones, with over 100,000 stars on GitHub.       
    **React is a JavaScript library - one of the most popular ones, with over 100,000 stars on GitHub.**       

  - React is / is not a framework (unlike Angular, which is more opinionated).     
    **React is not a framework (unlike Angular, which is more opinionated.**   

  - React is an .......... project created by .......... .   
    **React is an open-source project created by Facebook.**   

  - React is used to build .......... on the front end.   
    **React is used to build user interfaces (UI) on the front end.**   

  - React is the .......... layer of an MVC application (Model View Controller)   
    **React is the view layer of an MVC application (Model View Controlle**

## Quellen

* [Maven NPM/Node Fehler](<https://stackoverflow.com/questions/54581188/why-does-maven-install-give-this-error-with-npm>)
* [Spring React - Example](https://github.com/spring-guides/tut-react-and-spring-data-rest)
* [React Framework](https://reactjs.org/)
* [React - Filter Dependencies](https://github.com/babel/babel/issues/8655)
* [React - Filtern](https://stackoverflow.com/questions/35582978/best-way-to-filter-table-in-react)
* [React - If in Render Methode](https://reactjs.org/docs/conditional-rendering.html)
* [React - Mehrere Elemente Rendern](https://stackoverflow.com/questions/34893506/return-multiple-elements-inside-react-render)
* [React - State eines Objektes ändern](https://stackoverflow.com/questions/29537299/react-how-do-i-update-state-item1-on-setstate-with-jsfiddle)
* [Javascript - Methode includes](https://www.w3schools.com/jsref/jsref_includes.asp)
* [React Bootstrap](https://react-bootstrap.github.io/getting-started/introduction/)
* [Getting started with React](https://www.taniarascia.com/getting-started-with-react/)
* [Allgemeines über React](https://de.wikipedia.org/wiki/React)

